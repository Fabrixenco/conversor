package dam.android.fabricio.u2p4conversor;



import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

public class LogActivity extends AppCompatActivity {
    private static final String DEBUG_TAG="LOG-";

    private void  notify(String eventName){
String activityName = this.getClass().getSimpleName();
        String CHANEL_ID="My_LifeCycle";

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            NotificationChannel notificationChannel = new NotificationChannel(CHANEL_ID, "My Lifecycle",
                    NotificationManager.IMPORTANCE_DEFAULT);
            notificationChannel.setDescription("Lifecycle events");
            notificationChannel.setShowBadge(true);

            NotificationManager notificationManager = getSystemService(NotificationManager.class);

            if (notificationManager !=null){
                notificationManager.createNotificationChannel(notificationChannel);
            }

        }

        NotificationManagerCompat notificationManagerCompat = NotificationManagerCompat.from(this);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, CHANEL_ID).setContentTitle(eventName +" "+ activityName).setContentText(getPackageName()).setAutoCancel(true).setSmallIcon(R.mipmap.ic_launcher);

        notificationManagerCompat.notify((int) System.currentTimeMillis(), notificationBuilder.build());

    }
    protected void onConverter() {
        Log.i(DEBUG_TAG,"Boton de Conversion");
    }
    protected void onBorrar(){
        Log.i(DEBUG_TAG,"Boton de borrar");
    }

    protected void onStart() {
        super.onStart();
        Log.i(DEBUG_TAG,"onStart");
        notify("onStart");
    }



    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i(DEBUG_TAG,"onDestroy");
        notify("onDestroy");
        if(isFinishing()){
            Log.i(DEBUG_TAG,"Es de Usuario");
        }else{
            Log.i(DEBUG_TAG,"Es de Sistema");
        }
    }


    @Override
    protected void onStop() {
        super.onStop();
        Log.i(DEBUG_TAG,"onStop");
        notify("onStop");

    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i(DEBUG_TAG,"onPause");
        notify("onPause");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i(DEBUG_TAG,"onResume");
        notify("onResume");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.i(DEBUG_TAG,"onRestart");
        notify("onRestart");
    }



}

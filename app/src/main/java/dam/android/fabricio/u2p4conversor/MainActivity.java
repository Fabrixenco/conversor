package dam.android.fabricio.u2p4conversor;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends LogActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setUI();
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        //Ex4  Cogemos el text del error y le pasamos el mensaje guardado
        TextView errorText = findViewById(R.id.textViewError);
        errorText.setText(savedInstanceState.getString("mensaje"));
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        //Ex4 guardamos el mensaje del error
        outState.putString("mensaje", "Valor negativo incorrecto");
    }

    private void setUI() {
        final EditText etPulgada = findViewById(R.id.et_Pulgada);
        // TODO: Ex1 Two-wayconversion inch <-> cm cambiamos nombre de etResultado por Centimetros

        final EditText etCentimetros = findViewById(R.id.et_Resultado);

        //TextView del error
        final TextView errorText = findViewById(R.id.textViewError);
        Button buttonConvertir = findViewById(R.id.button_Convertir);
        Button buttonBorrar = findViewById(R.id.button_Borrar);

        // TODO: Ex1 Two-wayconversion inch <-> cm hacemos que el nuevo boton borrar borre todo para nueva conversion
        buttonBorrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etPulgada.setText("");
                etCentimetros.setText("");
                onBorrar();
            }
        });

        buttonConvertir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onConverter();
                try {
                    // TODO: Ex1 Two-wayconversion inch <-> cm comprobacion de que si tenemos algo en pulgadas o en centimetros haga el calculo
                    if ((etPulgada.getText().toString().length() > 0) && (etCentimetros.getText().toString().length() == 0)) {

                        try {

                            etCentimetros.setText(convertir(etPulgada.getText().toString(), 1));
                        } catch (Exception e) {
                            errorText.setText(e.getMessage());
                        }
                    }
                    if ((etCentimetros.getText().toString().length() > 0) && (etPulgada.getText().toString().length() == 0)) {

                        try {
                            etPulgada.setText(convertir(etCentimetros.getText().toString(), 2));
                        } catch (Exception e) {
                            errorText.setText(e.getMessage());
                        }
                    }


                } catch (Exception e) {
                    Log.e("LogsConversor", e.getMessage());
                }
            }
        });
    }

    private String convertir(String medidaText, int opcion) throws Exception {
        String valor = "";
        if (Double.parseDouble(medidaText) < 1) {
            throw new Exception("Valor negativo incorrecto");
        }

        if (opcion == 1) {


            valor = String.format("%.2f", Double.parseDouble(medidaText) * 2.54);

        }
        if (opcion == 2) {
            valor = String.format("%.2f", Double.parseDouble(medidaText) / 2.54);

        }


        return valor;
    }

}

